//IMPORT CORE LIBRARY
var library = require("./app/core/library");

//INITIALIZE EXPRESS
const app = library.express();

const server = library.http.createServer(app);

//IMPORT ROUTES
const webRoutes = require(__dirname + "/app/routes/web.routes");
const appRoutes = require(__dirname + "/app/routes/api.routes");

//BODYPARSER CONFIG
app.use(
	library.bodyParser.json({
		limit: library.config.limitBody,
	})
);
app.use(
	library.bodyParser.urlencoded({
		extended: true,
		limit: library.config.limitBody,
	})
);
//END BODYPARSER CONFIG

//CORS CONFIG
var whitelists = library.config.whitelist;
const corsOptions = (req, callback) => {
	if (whitelists.indexOf(req.header("host")) !== -1) {
		callback(null, true);
	} else {
		callback(new Error(library.messages.notAllowedCors));
	}
};
app.use(library.cors(corsOptions));
//END CORS CONFIG

app.use(
	library.session({
		cookie: {
			maxAge: Date.now() + 1000 * 60 * 60 * 24,
		},
		secret: library.config.jwtSecret,
		resave: true,
		saveUninitialized: true,
	})
);

app.use(library.cookieParser());

app.use(library.expressip().getIpInfoMiddleware);
app.use(library.device.capture());

library.device.enableViewRouting(app);

var csrfProtection = library.csrf({
	cookie: true,
});

app.use(
	"/assets",
	library.express.static(__dirname + "/public/assets", {
		etag: false,
	})
);

app.use(
	"/assets",
	library.express.static(__dirname + "/public/assets", {
		etag: false,
	})
);

app.use("/api/", csrfProtection, appRoutes);
app.use("/", csrfProtection, webRoutes);

//CONFIG OF HBS
library.hbs.registerPartials(__dirname + "/public/templates/includes");
library.hbs.registerPartials(__dirname + "/public/templates/pages");

library.hbsutils.registerWatchedPartials(
	__dirname + "/public/templates/includes"
);
library.hbsutils.registerWatchedPartials(__dirname + "/public/templates/pages");
library.hbsutils.precompilePartials();

app.set("views", library.path.join(__dirname, "/public/templates"));
library.hbs.registerHelper("defaultLayout", () => "template.hbs");
app.disable("views cache");
app.set("view engine", "hbs");

library.hbs.registerHelper("ifEquals", function (arg1, arg2, options) {
	return arg1 == arg2 ? options.fn(this) : options.inverse(this);
});
library.hbs.registerHelper("ifNotEquals", function (arg1, arg2, options) {
	return arg1 != arg2 ? options.fn(this) : options.inverse(this);
});

library.hbs.registerHelper("ifAnd", function (v1, v2, options) {
	if (v1 === v2) {
		return options.fn(this);
	}
	return options.inverse(this);
});

library.hbs.registerHelper("inc", function (value, options) {
	return parseInt(value) + 1;
});

library.hbs.registerHelper("cari", function (data, url) {
	for (const item of data.role_group.role_group_has_menu) {
		if (item.menus && item.menus.url === url) {
			for (const access of item.menus.access) {
				if (access.access_menu.access_name === "read") {
					return true;
				}
			}
		}
		if (item.menus && Array.isArray(item.menus.sub_menus)) {
			for (const subMenu of item.menus.sub_menus) {
				if (subMenu.url === url) {
					for (const access of subMenu.access) {
						if (access.access_menu.access_name === "read") {
							return true;
						}
					}
				}
			}
		}
	}
	return false;
});

library.hbs.registerHelper("log", function (content) {
	console.log(content.fn(this));
	return "";
});

library.hbs.registerHelper("getFirstLetter", function (name) {
	return name.match(/\b\w/g).join("");
});

library.hbs.registerHelper("selisih", function (tglKembali) {
	const currentDate = library.moment();
	const formattedDate = currentDate.format("YYYY-MM-DD HH:mm:ss");

	const tglAwal = library.moment(tglKembali);
	const tglAkhir = library.moment(formattedDate);
	let numYears, numMonths, numDays;

	numYears = tglAkhir.diff(tglAwal, "years");
	tglAwal.add(numYears, "years");
	numMonths = tglAkhir.diff(tglAwal, "months");
	tglAwal.add(numMonths, "months");
	numDays = tglAkhir.diff(tglAwal, "days");

	if (numYears === 0 && numMonths === 0) {
		if (numDays === 0) {
			return "Aktif 1 hari lalu";
		} else {
			return `Aktif ${numDays} hari lalu`;
		}
	} else if (numYears === 0 && numMonths !== 0) {
		if (numDays === 0) {
			return `Aktif ${numMonths} Bulan lalu`;
		} else {
			return `Aktif ${numMonths} Bulan, ${numDays} hari`;
		}
	} else if (numYears !== 0) {
		if (numMonths === 0 && numDays === 0) {
			return `Aktif ${numYears} Tahun`;
		} else if (numMonths !== 0 && numDays === 0) {
			return `Aktif ${numYears} Tahun, ${numMonths} Bulan`;
		} else if (numMonths === 0 && numDays !== 0) {
			return `Aktif ${numYears} Tahun, ${numDays} Hari`;
		} else if (numMonths !== 0 && numDays !== 0) {
			return `Aktif ${numYears} Tahun, ${numMonths} Bulan, ${numDays} Hari`;
		}
	}
});

library.hbs.registerHelper("truncate", function (text, length) {
	words = text.split(" ");
	new_text = text;
	if (words.length > length) {
		new_text = "";
		for (var i = 0; i <= length; i++) {
			new_text += words[i] + " ";
		}
		new_text = new_text.trim() + "...";
	}
	return new_text;
});

library.hbs.registerHelper("toRupiah", function (angka, prefix) {
	let angka1 = angka.toString();
	let number_string = angka1.replace(/[^,\d]/g, "").toString();
	var split = number_string.split(",");
	var sisa = split[0].length % 3;
	var rupiah = split[0].substr(0, sisa);
	var ribuan = split[0].substr(sisa).match(/\d{3}/gi);

	if (ribuan) {
		separator = sisa ? "." : "";
		rupiah += separator + ribuan.join(".");
	}

	rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
	return prefix == undefined ? rupiah : rupiah ? "Rp " + rupiah : "";
});

app.use(async function (err, req, res, next) {
	console.log(err);
	return new Promise(async (resolve, reject) => {
		try {
			if (err.code == library.messages.badCsrfToken) {
				return res.status(library.responseStatus.serverError).json({
					success: false,
					message: library.messages.badCsrfToken,
					status: 404,
				});
			} else if (err.message == library.messages.notAllowedCors) {
				return res.status(library.responseStatus.serverError).json({
					success: false,
					message: library.messages.dontHaveAccess,
					status: 404,
				});
			}
		} catch (error) {
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
});

const startServer = server.listen(library.config.port);

if (startServer) {
	console.log(`${library.messages.messageRunServer} ${library.config.port}`);
}
