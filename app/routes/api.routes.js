const express = require("express");

const route = express.Router();

const webSetting = require("../middleware/web_setting");
const authMiddleware = require("../middleware/auth.middleware");

const authController = require("../controller/auth.controller");
const userController = require("../controller/user.controller");
const roleGroupController = require("../controller/role_group.controller");
const menuController = require("../controller/menu.controller");

route.post(
	"/auth/login",
	authMiddleware.mode,
	webSetting.web_setting,
	authController.login_action
);

route.put(
	"/user/profile",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	userController.update_profile
);

route.delete(
	"/user/:id/delete_device",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	userController.delete_device
);

//ROLE GROUP CONTROLLER
route.get(
	"/role_group",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	roleGroupController.get_role_group
);

route.post(
	"/role_group",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	roleGroupController.add_role_group
);

route.delete(
	"/role_group/:id",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	roleGroupController.delete_role_group
);

route.get(
	"/role_group/:id",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	roleGroupController.role_group_by_id
);

route.put(
	"/role_group/:id",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	roleGroupController.update_role_group
);

route.get(
	"/role_group/permission/:id",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	roleGroupController.permission_by_id
);

//MENU API
route.get(
	"/menu",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	menuController.get_menu
);

route.post(
	"/menu",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	menuController.add_menu
);

route.delete(
	"/menu/:id",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	menuController.delete_menu
);

route.get(
	"/menu/:id",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	menuController.menu_by_id
);

route.put(
	"/menu/:id",
	authMiddleware.mode,
	webSetting.web_setting,
	authMiddleware.unauth,
	menuController.update_menu
);

module.exports = route;
