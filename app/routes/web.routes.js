const express = require("express");

const route = express.Router();

const webSetting = require("../middleware/web_setting");
const authMiddleware = require("../middleware/auth.middleware");

const mainController = require("../controller/main.controller");
const dashboardController = require("../controller/dashboard.controller");

route.get(
	["/", "/login"],
	webSetting.web_setting,
	authMiddleware.auth,
	mainController.main_pages
);

route.get(
	":path(*)",
	webSetting.web_setting,
	authMiddleware.unauth,
	dashboardController.dashboard_pages
);

module.exports = route;
