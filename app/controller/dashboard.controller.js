const library = require("../core/library");

exports.dashboard_pages = async (req, res) => {
	try {
		await library.formData(req, res);

		let route = req.url.split("/");
		let navigator;

		let pages = req.query;
		let dataDashboard = {};
		let permission = {};
		let token = req.session.tokenLogin;
		let payload = req.decrypted.payload;

		if (library.isEmpty(route[1]) || route[1] == "dashboard") {
			navigator = "dashboard";
		} else {
			navigator = route[1];
		}

		var fileContents;
		var showPages, showCss, showJs, showContent;

		const user_has_role = await library.model.user_has_role.findOne({
			include: [
				{
					model: library.model.profile,
					as: "profile",
					include: [
						{
							model: library.model.sessions,
							as: "session",
							include: [
								{
									model: library.model.auth_device,
									as: "device",
								},
							],
						},
					],
				},
				{
					model: library.model.role_group,
					as: "role_group",
					include: [
						{
							model: library.model.role_group_has_menu,
							as: "role_group_has_menu",
							include: [
								{
									model: library.model.menu,
									as: "menus",
									include: [
										{
											model: library.model.sub_menu,
											include: [
												{
													model: library.model.sub_sub_menu,
												},
												{
													model: library.model.sub_menu_has_acces,
													as: "access",
													include: [
														{
															model: library.model.access_menu,
															as: "access_menu",
														},
													],
												},
											],
										},
										{
											model: library.model.menu_has_access,
											as: "access",
											include: [
												{
													model: library.model.access_menu,
													as: "access_menu",
												},
											],
										},
									],
								},
							],
						},
					],
				},
			],
			where: {
				id_auth: await library.decrypt(payload.user_login_auth),
			},
			order: [
				[
					{ model: library.model.role_group, as: "role_group" },
					{
						model: library.model.role_group_has_menu,
						as: "role_group_has_menu",
					},
					"order_menu",
					"ASC",
				],
			],
		});

		var objectdlu = JSON.stringify(user_has_role);
		var newDashboard = JSON.parse(objectdlu);

		await library.asyncForEach(
			user_has_role.profile.session,
			async (element, index) => {
				await library.waitFor(1);
				if (element.session_token == token) {
					newDashboard.profile.session[index].current_active = true;
				} else {
					newDashboard.profile.session[index].current_active = false;
				}
			}
		);

		if (library.searchUrlWithAccess(newDashboard, req.url)) {
			try {
				library.fs
					.readdirSync(`public/templates/pages/${navigator}`)
					.forEach((file) => {
						showPages = `${navigator}/index`;
						showJs = `${navigator}/js`;
						showCss = `${navigator}/css`;
						showContent = `${navigator}/content`;
					});
			} catch (err) {
				showPages = "error/403";
				showCss = `error/no_item`;
				showJs = `error/no_item`;
				showContent = `error/no_item`;
			}
		} else {
			showPages = "error/403";
			showCss = `error/no_item`;
			showJs = `error/no_item`;
			showContent = `error/no_item`;
		}

		return res.render("template", {
			csrfToken: req.csrfToken(),
			web_title: "Admin Pro - " + req.web_setting.web_name,
			web_desc: req.web_setting.web_desc,
			web_author: req.web_setting.web_author,
			web_keywords: req.web_setting.web_keywords,
			web_url: `${req.web_setting.web_url}/${navigator}`,
			web_icon: req.web_setting.web_icon,
			web_logo: req.web_setting.web_logo,
			datas: newDashboard,
			content: function (err) {
				return showPages;
			},
			includeCss: function () {
				return showCss;
			},
			includeJs: function () {
				return showJs;
			},
			_content: function (err) {
				return showContent;
			},
		});
	} catch (error) {
		console.log(error);
	}
};
