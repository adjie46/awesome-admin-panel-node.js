const library = require("../core/library");

exports.get_menu = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			query = JSON.parse(JSON.stringify(req.query));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			let { draw, start, length, search } = req.query;
			let pageNumber = 1;

			let array = [];

			if (draw == 1) {
				pageNumber = 1;
			} else {
				pageNumber = 1;
				pageNumber += start++;
			}

			if (!library.isEmpty(search.value)) {
				start = req.query.start;
			} else {
				start = req.query.start;
				search.value = ""; // Set search.value to an empty string when it's falsy
			}

			const datas = await library.model.menu.findAll({
				offset: parseInt(start),
				limit: parseInt(length),
				order: [["createdAt", "ASC"]],
				where: library.Sequelize.where(
					library.Sequelize.fn("LOWER", library.Sequelize.col("name")), // Konversi kolom 'username' ke huruf kecil
					{
						[library.Op.substring]: search.value, // Matches 'john' case-insensitively
					}
				),
			});

			const totalRecords = await library.model.menu.count();

			await library.asyncForEach(datas, async (element, index) => {
				await library.waitFor(1);
				array.push([
					`${element.id}`,
					`${element.name}`,
					`${element.url}`,
					`${element.icon}`,
					`${pageNumber++}`,
				]);
			});

			return res.status(200).json({
				draw: draw,
				recordsTotal: totalRecords,
				recordsFiltered: datas.length,
				data: array,
			});
		} catch (error) {
			console.log(error);
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.add_menu = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			if (library.isEmpty(datas.menu_name)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.menu_name_is_require,
					redirect: "/menu",
				});
			} else {
				newMenu = {
					name: datas.menu_name,
					url: datas.url_menu,
					icon: datas.icon_menu,
				};

				const [menu, created] = await library.model.menu.findOrCreate({
					where: {
						name: datas.menu_name,
					},
					defaults: newMenu,
				});

				if (created) {
					const fileData = {
						contentHbs: "",
						cssHbs: "",
						indexHbs: `
						<body data-pc-preset="preset-1" data-pc-sidebar-caption="true" data-pc-direction="ltr" data-pc-theme_contrast=""
							data-pc-theme="light">
							<!-- [ Pre-loader ] start -->
							<div class="loader-bg">
								<div class="loader-track">
									<div class="loader-fill"></div>
								</div>
							</div>
							<!-- [ Pre-loader ] End -->
							<!-- [ Sidebar Menu ] start -->
							{{> sidebar}}
							<!-- [ Sidebar Menu ] end --> <!-- [ Header Topbar ] start -->
							{{> top_nav}}
							<!-- [ Header ] end -->



							<!-- [ Main Content ] start -->
							<div class="pc-container">
								<div class="pc-content">
									{{> (_content)}}
								</div>

							</div>
							<!-- [ Main Content ] end -->
							{{> footer}}

						</body>
						`,
						jsHbx: "",
					};
					library.createFolder(
						`public/templates/pages/${datas.url_menu.split("/")[1]}`,
						fileData
					);

					return res.status(library.responseStatus.OK).send({
						success: true,
						code: library.responseStatus.OK,
						message: library.messages.menu_created,
						redirect: "/menu",
					});
				} else {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: library.messages.menu_not_created,
						redirect: "/menu",
					});
				}
			}
		} catch (error) {
			library.logger.error("Add Menu:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.delete_menu = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			id = req.params.id;

			menus = await library.model.menu.findOne({
				where: {
					id: id,
				},
			});

			await library.model.menu_has_access.destroy({
				where: {
					id_menu: id,
				},
			});

			await library.model.role_group_has_menu.destroy({
				where: {
					id_menu: id,
				},
			});

			delete_route_group = await library.model.menu.destroy({
				where: {
					id: id,
				},
			});

			if (delete_route_group) {
				library.deleteFolder(
					`public/templates/pages/${menus.url.split("/")[1]}`
				);
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: library.messages.menu_deleted,
					redirect: "/roles_permission",
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.menu_not_deleted,
					redirect: "/roles_permission",
				});
			}
		} catch (error) {
			console.log(error);
			/* library.logger.error("Delete Menu:", error); */
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.menu_by_id = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			id = req.params.id;

			menu = await library.model.menu.findOne({
				where: {
					id: id,
				},
			});

			if (library.isEmpty(menu)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.data_not_found,
					redirect: "/menu",
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: library.messages.data_found,
					redirect: "/menu",
					data: menu,
				});
			}
		} catch (error) {
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.update_menu = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));
			id = req.params.id;

			if (library.isEmpty(datas.edt_menu_name)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.menu_name_is_require,
					redirect: "/menu",
				});
			} else {
				updateMenu = {
					name: datas.edt_menu_name,
					url: datas.edt_url_menu,
					icon: datas.icon_menu,
				};

				const update = await library.model.menu.update(updateMenu, {
					where: {
						id: id,
					},
				});

				if (update) {
					return res.status(library.responseStatus.OK).send({
						success: true,
						code: library.responseStatus.OK,
						message: library.messages.menu_updated,
						redirect: "/menu",
					});
				} else {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: library.messages.menu_not_updated,
						redirect: "/menu",
					});
				}
			}
		} catch (error) {
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
