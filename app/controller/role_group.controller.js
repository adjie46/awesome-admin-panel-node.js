const library = require("../core/library");

exports.get_role_group = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			query = JSON.parse(JSON.stringify(req.query));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			let { draw, start, length, search } = req.query;
			let pageNumber = 1;

			let array = [];

			if (draw == 1) {
				pageNumber = 1;
			} else {
				pageNumber = 1;
				pageNumber += start++;
			}

			if (!library.isEmpty(search.value)) {
				start = req.query.start;
			} else {
				start = req.query.start;
				search.value = ""; // Set search.value to an empty string when it's falsy
			}

			const datas = await library.model.role_group.findAll({
				offset: parseInt(start),
				limit: parseInt(length),
				order: [["createdAt", "ASC"]],
				where: library.Sequelize.where(
					library.Sequelize.fn("LOWER", library.Sequelize.col("role_name")), // Konversi kolom 'username' ke huruf kecil
					{
						[library.Op.substring]: search.value, // Matches 'john' case-insensitively
					}
				),
			});

			const totalRecords = await library.model.role_group.count();

			await library.asyncForEach(datas, async (element, index) => {
				await library.waitFor(1);
				array.push([
					`${element.id}`,
					`${element.role_name}`,
					`${pageNumber++}`,
				]);
			});

			return res.status(200).json({
				draw: draw,
				recordsTotal: totalRecords,
				recordsFiltered: datas.length,
				data: array,
			});
		} catch (error) {
			console.log(error);
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.delete_role_group = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			id = req.params.id;

			delete_route_group = await library.model.role_group.destroy({
				where: {
					id: id,
				},
			});

			if (delete_route_group) {
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: library.messages.role_group_deleted,
					redirect: "/roles_permission",
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.role_group_not_deleted,
					redirect: "/roles_permission",
				});
			}
		} catch (error) {
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.add_role_group = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			if (library.isEmpty(datas.role_group)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.role_group_is_require,
					redirect: "/roles_permission",
				});
			} else {
				newRoleGroup = {
					role_name: datas.role_group,
				};

				const [roleGroup, created] =
					await library.model.role_group.findOrCreate({
						where: {
							role_name: datas.role_group,
						},
						defaults: newRoleGroup,
					});

				if (created) {
					return res.status(library.responseStatus.OK).send({
						success: true,
						code: library.responseStatus.OK,
						message: library.messages.role_group_created,
						redirect: "/roles_permission",
					});
				} else {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: library.messages.role_group_not_created,
						redirect: "/roles_permission",
					});
				}
			}
		} catch (error) {
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.update_role_group = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));
			id = req.params.id;

			if (library.isEmpty(datas.edt_role_group)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.role_group_is_require,
					redirect: "/roles_permission",
				});
			} else {
				newRoleGroup = {
					role_name: datas.edt_role_group,
				};

				const update = await library.model.role_group.update(newRoleGroup, {
					where: {
						id: id,
					},
				});

				if (update) {
					return res.status(library.responseStatus.OK).send({
						success: true,
						code: library.responseStatus.OK,
						message: library.messages.role_group_updated,
						redirect: "/roles_permission",
					});
				} else {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: library.messages.role_group_not_updated,
						redirect: "/roles_permission",
					});
				}
			}
		} catch (error) {
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.role_group_by_id = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			id = req.params.id;

			role_group = await library.model.role_group.findOne({
				attributes: ["id", "role_name"],
				where: {
					id: id,
				},
			});

			if (library.isEmpty(role_group)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.data_not_found,
					redirect: "/roles_permission",
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: library.messages.data_found,
					redirect: "/roles_permission",
					data: role_group,
				});
			}
		} catch (error) {
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.permission_by_id = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			id = req.params.id;

			permission = await library.model.role_group.findOne({
				attributes: ["id", "role_name"],
				include: [
					{
						model: library.model.role_group_has_menu,
						as: "role_group_has_menu",
						include: [
							{
								model: library.model.menu,
								as: "menus",
								include: [
									{
										model: library.model.sub_menu,
										include: [
											{
												model: library.model.sub_sub_menu,
											},
											{
												model: library.model.sub_menu_has_acces,
												as: "access",
												include: [
													{
														model: library.model.access_menu,
														as: "access_menu",
													},
												],
											},
										],
									},
									{
										model: library.model.menu_has_access,
										as: "access",
										include: [
											{
												model: library.model.access_menu,
												as: "access_menu",
											},
										],
									},
								],
							},
						],
					},
				],
				where: {
					id: id,
				},
			});

			html = `
				<div class="col-xl-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>${permission.role_name}</h5>
                        </div>
                        <div class="card-body table-border-style">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Menu</th>
                                            <th>Create</th>
                                            <th>Read</th>
                                            <th>Update</th>
                                            <th>Delete</th>
                                            <th>Print</th>
                                            <th>Export</th>
                                            <th>Import</th>
                                            <th>Download</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       ${permission.role_group_has_menu
																					.map(
																						(perm, index) => `
                                <tr>
                                    <td>${index + 1}</td>
                                    <td>${perm.menus.name}</td>
                                    ${[
																			"CREATE",
																			"READ",
																			"UPDATE",
																			"DELETE",
																			"PRINT",
																			"EXPORT",
																			"IMPORT",
																			"DOWNLOAD",
																		]
																			.map(
																				(accessName) => `
                                        <td>
                                            <div class="form-check mb-2">
                                                <input class="form-check-input input-success" type="checkbox" id="customCheckc3" ${
																									perm.menus.access.some(
																										(access) =>
																											access.access_menu.access_name.toUpperCase() ===
																											accessName
																									)
																										? "checked"
																										: ""
																								}>
                                            </div>
                                        </td>
                                    `
																			)
																			.join("")}
                                </tr>
                                ${
																	perm.menus.sub_menus
																		? perm.menus.sub_menus
																				.map(
																					(subMenu, subIndex) => `
                                            <tr>
                                                <td>${index + 1}.${
																						subIndex + 1
																					}</td>
                                                <td>${subMenu.name}</td>
                                                ${[
																									"CREATE",
																									"READ",
																									"UPDATE",
																									"DELETE",
																									"PRINT",
																									"EXPORT",
																									"IMPORT",
																									"DOWNLOAD",
																								]
																									.map(
																										(accessName) => `
                                                    <td>
                                                        <div class="form-check mb-2">
                                                            <input class="form-check-input input-success" type="checkbox" id="customCheckc3" ${
																															subMenu.access.some(
																																(access) =>
																																	access.access_menu.access_name.toUpperCase() ===
																																	accessName
																															)
																																? "checked"
																																: ""
																														}>
                                                        </div>
                                                    </td>
                                                `
																									)
																									.join("")}
                                            </tr>
                                        `
																				)
																				.join("")
																		: ""
																}
                            `
																					)
																					.join("")}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
			`;

			if (library.isEmpty(permission)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.data_not_found,
					data: "",
					redirect: "/roles_permission",
				});
			} else {
				return res.status(library.responseStatus.OK).send({
					success: true,
					code: library.responseStatus.OK,
					message: library.messages.data_found,
					redirect: "/roles_permission",
					data: html,
				});
			}
		} catch (error) {
			console.log(error);
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
