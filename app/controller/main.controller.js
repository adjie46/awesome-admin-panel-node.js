const library = require("../core/library");

exports.main_pages = async (req, res) => {
	try {
		await library.formData(req, res);

		let route = req.url.split("/");
		let navigator;
		let showCss, showJs, showPage;

		if (library.isEmpty(route[1]) || route[1] == "login") {
			navigator = "auth";
		} else {
			navigator = route[1];
		}

		try {
			fileContents = library.fs
				.readdirSync(`public/templates/pages/${navigator}`)
				.forEach((file) => {
					showPage = `${navigator}/index`;
					showJs = `${navigator}/js`;
					showCss = `${navigator}/css`;
				});
		} catch (err) {
			showPage = "error/404";
			showCss = `error/no_item`;
			showJs = `error/no_item`;
		}

		return res.render("template", {
			csrfToken: req.csrfToken(),
			web_title: "Login - " + req.web_setting.web_name,
			web_desc: req.web_setting.web_desc,
			web_author: req.web_setting.web_author,
			web_keywords: req.web_setting.web_keywords,
			web_url: `${req.web_setting.web_url}/${navigator}`,
			web_icon: req.web_setting.web_icon,
			web_logo: req.web_setting.web_logo,
			content: function (err) {
				return showPage;
			},
			includeCss: function () {
				return showCss;
			},
			includeJs: function () {
				return showJs;
			},
		});
	} catch (error) {
		let showCss, showJs, showPage;
		showPage = "error/505";
		showCss = `error/no_item`;
		showJs = `error/no_item`;

		return res.render("template", {
			csrfToken: req.csrfToken(),
			web_title: "Error 505 - " + req.web_setting.web_name,
			web_desc: req.web_setting.web_desc,
			web_author: req.web_setting.web_author,
			web_keywords: req.web_setting.web_keywords,
			web_url: `${req.web_setting.web_url}/login`,
			web_icon: req.web_setting.web_icon,
			web_logo: req.web_setting.web_logo,
			content: function (err) {
				return showPage;
			},
			includeCss: function () {
				return showCss;
			},
			includeJs: function () {
				return showJs;
			},
		});
	}
};
