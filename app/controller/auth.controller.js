const library = require("../core/library");

exports.login_action = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			datas = JSON.parse(JSON.stringify(req.body));

			users = await library.model.auth.findOne({
				include: [
					{
						model: library.model.profile,
						as: "profile",
					},
				],
				where: {
					[library.Op.or]: [
						{
							user_auth: datas.username_login,
						},
						{
							"$profile.email$": datas.username_login,
						},
					],
				},
				paranoid: false,
			});

			if (library.isEmpty(users)) {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: "Pengguna Tidak Ditemukan",
					redirect: "/login",
				});
			} else {
				if (users.status == 0) {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: "Pengguna Telah Dinonaktifkan",
						redirect: "/login",
					});
				} else if (users.status == 1) {
					passDecrypt = await library.decrypt(users.user_pass).catch((err) => {
						return res.status(library.responseStatus.serverError).send({
							success: false,
							code: library.responseStatus.serverError,
							message: "Error 101, Hubungi Admin",
						});
					});

					if (passDecrypt === datas.password_login) {
						var payload;
						if (users.user_type == "-1" || users.user_type == -1) {
							payload = {
								user_login_name: users.profile.full_name,
								user_login_auth: await library.encrypt(users.id),
								user_login_opd: "",
							};
						} else {
							payload = {
								user_login_name: users.profile.full_name,
								user_login_auth: await library.encrypt(users.id),
								user_login_opd: await library.encrypt(users.profile.opd_id),
							};
						}

						const secret = Buffer.from(library.config.hex, "hex");

						const encryptedJwt = await library.encryptedJwt(
							"login_action",
							payload,
							secret,
							library.config.jwtExpireWeb
						);

						session = req.session;
						session.tokenLogin = "Bearer " + encryptedJwt;

						check_device = await library.model.auth_device.findOne({
							where: {
								[library.Op.and]: [
									{
										id_auth: users.id,
									},
									{
										ip_address: req.ipInfo.ip,
									},
								],
							},
						});

						//TAMBAH BARU
						if (library.isEmpty(check_device)) {
							newDevice = {
								id: await library.uuid(),
								id_auth: users.id,
								imei_device: (await library.si.system()).uuid,
								ip_address: req.ipInfo.ip,
								hardware: (await library.si.system()).model,
								manufacturer: (await library.si.cpu()).manufacturer,
								type: (await library.si.cpu()).brand,
								os_name: (await library.si.osInfo()).codename,
								status: 1,
							};

							const [device, created] =
								await library.model.auth_device.findOrCreate({
									where: {
										[library.Op.and]: [
											{
												id_auth: users.id,
											},
											{
												ip_address: req.ipInfo.ip,
											},
										],
									},
									defaults: newDevice,
								});

							if (created) {
								newSession = {
									id_auth: users.id,
									id_device: device.id,
									session_token: "Bearer " + encryptedJwt,
									ip_address: req.ipInfo.ip,
									login_time: library.currentDateTime(),
									logout_time: null,
								};

								await library.model.sessions.create(newSession);
							}
						} else {
							check_sesion = await library.model.sessions.findOne({
								where: {
									[library.Op.and]: [
										{
											id_device: check_device.id,
										},
										{
											ip_address: req.ipInfo.ip,
										},
									],
								},
							});

							if (!library.isEmpty(check_sesion)) {
								newSession = {
									id_auth: users.id,
									id_device: check_device.id,
									session_token: "Bearer " + encryptedJwt,
									ip_address: req.ipInfo.ip,
									login_time: library.currentDateTime(),
									logout_time: null,
								};
								await library.model.sessions.update(newSession, {
									where: {
										[library.Op.and]: [
											{
												id_device: check_device.id,
											},
											{
												ip_address: req.ipInfo.ip,
											},
										],
									},
								});

								newDevice = {
									imei_device: (await library.si.system()).uuid,
									hardware: (await library.si.system()).model,
									manufacturer: (await library.si.cpu()).manufacturer,
									type: (await library.si.cpu()).brand,
									os_name: (await library.si.osInfo()).codename,
									status: 1,
								};
								await library.model.auth_device.update(newDevice, {
									where: {
										id: check_device.id,
									},
								});
							} else {
								newSession = {
									id_auth: users.id,
									id_device: check_device.id,
									session_token: "Bearer " + encryptedJwt,
									ip_address: req.ipInfo.ip,
									login_time: library.currentDateTime(),
									logout_time: null,
								};

								await library.model.sessions.create(newSession);
							}
						}

						newLogs = {
							id_auth: await library.encrypt(users.id),
							log_type: "Success",
							log_action: `${users.profile.full_name} berhasil login`,
							log_os: req.headers["sec-ch-ua-platform"],
							log_devices: req.device.type,
							log_ip: req.ipInfo.ip,
						};

						library.logs(newLogs);

						return res.status(library.responseStatus.OK).send({
							success: true,
							code: library.responseStatus.OK,
							message: "Berhasil Masuk",
							redirect: "/dashboard",
						});
					} else {
						newLogs = {
							id_auth: await library.encrypt(users.id),
							log_type: "Success",
							log_action: `${users.profile.full_name} Gagal Melakukan Login, Error : Password Salah`,
							log_os: req.headers["sec-ch-ua-platform"],
							log_devices: req.device.type,
							log_ip: req.ipInfo.ip,
						};

						library.logs(newLogs);

						return res.status(library.responseStatus.OK).send({
							success: false,
							code: library.responseStatus.OK,
							message: "Password Anda Salah, Silahkan Coba Lagi",
							redirect: "/login",
						});
					}
				}
			}
		} catch (error) {
			console.log(error);
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
