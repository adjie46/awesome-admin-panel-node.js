const library = require("../core/library");
const { error_101 } = require("../core/message_response");

exports.update_profile = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));

			const passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,}/;

			if (
				passwordRegex.test(datas.new_password) ||
				passwordRegex.test(datas.confirm_password)
			) {
				userId = await library.decrypt(payload.user_login_auth).catch((err) => {
					return res.status(library.responseStatus.serverError).send({
						success: false,
						code: library.responseStatus.serverError,
						message: error_101,
					});
				});

				users = await library.model.auth.findOne({
					attributes: ["user_pass"],
					where: {
						id: userId,
					},
				});

				oldPass = await library.decrypt(users.user_pass).catch((err) => {
					return res.status(library.responseStatus.serverError).send({
						success: false,
						code: library.responseStatus.serverError,
						message: error_101,
					});
				});

				if (oldPass == datas.old_password) {
					if (datas.new_password == datas.confirm_password) {
						newPass = {
							user_pass: await library.encrypt(datas.confirm_password),
						};

						updatePass = await library.model.auth.update(newPass, {
							where: {
								id: userId,
							},
						});

						if (updatePass) {
							return res.status(library.responseStatus.OK).send({
								success: true,
								code: library.responseStatus.OK,
								message: library.messages.change_password_success,
								redirect: headers.referer,
							});
						} else {
							return res.status(library.responseStatus.OK).send({
								success: false,
								code: library.responseStatus.OK,
								message: library.messages.change_password_fail,
								redirect: headers.referer,
							});
						}
					} else {
						return res.status(library.responseStatus.OK).send({
							success: false,
							code: library.responseStatus.OK,
							message: library.messages.new_conf_password_wrong,
							redirect: headers.referer,
						});
					}
				} else {
					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: library.messages.old_password_wrong,
						redirect: headers.referer,
					});
				}
			} else {
				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.bad_combination_password,
					redirect: headers.referer,
				});
			}
		} catch (error) {
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};

exports.delete_device = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		try {
			await library.formData(req, res);
			headers = JSON.parse(JSON.stringify(req.headers));
			datas = JSON.parse(JSON.stringify(req.body));
			params = JSON.parse(JSON.stringify(req.params));
			payload = JSON.parse(JSON.stringify(req.decrypted.payload));
			session = req.session.tokenLogin;
			logout = false;

			userId = await library.decrypt(payload.user_login_auth).catch((err) => {
				return res.status(library.responseStatus.serverError).send({
					success: false,
					code: library.responseStatus.serverError,
					message: error_101,
				});
			});

			check_session = await library.model.sessions.findOne({
				where: {
					[library.Op.and]: [
						{
							id_device: params.id,
						},
						{
							id_auth: userId,
						},
					],
				},
			});

			devices = await library.model.auth_device.findOne({
				where: {
					id: params.id,
				},
			});

			if (library.isEmpty(check_session)) {
				newLogs = {
					id_auth: await library.encrypt(userId),
					log_type: "Gagal",
					log_action: `${payload.user_login_name} Gagal Logout Device`,
					log_os: req.headers["sec-ch-ua-platform"],
					log_devices: req.device.type,
					log_ip: req.ipInfo.ip,
				};

				library.logs(newLogs);

				return res.status(library.responseStatus.OK).send({
					success: false,
					code: library.responseStatus.OK,
					message: library.messages.logout_fail,
					redirect: headers.referer,
				});
			} else {
				logout = await library.model.sessions.destroy({
					where: {
						[library.Op.and]: [
							{
								id_device: params.id,
							},
							{
								id_auth: userId,
							},
						],
					},
				});

				device = await library.model.auth_device.destroy({
					where: {
						[library.Op.and]: [
							{
								id: params.id,
							},
							{
								id_auth: userId,
							},
						],
					},
				});

				if (logout && devices.ip_address == req.ipInfo.ip) {
					newLogs = {
						id_auth: await library.encrypt(userId),
						log_type: "Berhasil",
						log_action: `${payload.user_login_name} Berhasil Logout Device`,
						log_os: req.headers["sec-ch-ua-platform"],
						log_devices: req.device.type,
						log_ip: req.ipInfo.ip,
					};

					library.logs(newLogs);

					req.session.destroy((err) => {});

					return res.status(library.responseStatus.OK).send({
						success: true,
						code: library.responseStatus.OK,
						message: library.messages.logout_success,
						redirect: "/login",
					});
				} else if (logout && check_session.ip_address != req.ipInfo.ip) {
					newLogs = {
						id_auth: await library.encrypt(userId),
						log_type: "Berhasil",
						log_action: `${payload.user_login_name} Berhasil Logout Device`,
						log_os: req.headers["sec-ch-ua-platform"],
						log_devices: req.device.type,
						log_ip: req.ipInfo.ip,
					};

					library.logs(newLogs);

					return res.status(library.responseStatus.OK).send({
						success: true,
						code: library.responseStatus.OK,
						message: library.messages.logout_success,
						redirect: headers.referer,
					});
				} else {
					newLogs = {
						id_auth: await library.encrypt(userId),
						log_type: "Gagal",
						log_action: `${payload.user_login_name} Gagal Logout Device`,
						log_os: req.headers["sec-ch-ua-platform"],
						log_devices: req.device.type,
						log_ip: req.ipInfo.ip,
					};

					library.logs(newLogs);

					return res.status(library.responseStatus.OK).send({
						success: false,
						code: library.responseStatus.OK,
						message: library.messages.logout_fail,
						redirect: headers.referer,
					});
				}
			}
		} catch (error) {
			library.logger.error("Login:", error);
			return res.status(library.responseStatus.serverError).send({
				success: false,
				code: library.responseStatus.serverError,
				message: error.message,
			});
		}
	});
};
