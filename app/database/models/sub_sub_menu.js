"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class sub_sub_menu extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}
	sub_sub_menu.init(
		{
			name: DataTypes.STRING,
			url: DataTypes.STRING,
			icon: DataTypes.STRING,
			SubmenuId: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: "sub_sub_menu",
		}
	);
	return sub_sub_menu;
};
