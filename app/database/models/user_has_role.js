"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class user_has_role extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			user_has_role.belongsTo(models.profile, {
				foreignKey: "id_auth",
				targetKey: "id_auth",
				as: "profile",
				onDelete: "CASCADE",
				hooks: true,
			});
			user_has_role.belongsTo(models.role_group, {
				foreignKey: "id_role_group",
				targetKey: "id",
				as: "role_group",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	user_has_role.init(
		{
			id_auth: DataTypes.STRING,
			id_role_group: DataTypes.BIGINT,
		},
		{
			sequelize,
			modelName: "user_has_role",
		}
	);
	return user_has_role;
};
