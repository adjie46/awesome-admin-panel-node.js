"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class role_group_has_menu extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			role_group_has_menu.belongsTo(models.menu, {
				foreignKey: "id_menu",
				targetKey: "id",
				as: "menus",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	role_group_has_menu.init(
		{
			id_role_group: DataTypes.BIGINT,
			id_menu: DataTypes.INTEGER,
			order_menu: DataTypes.BIGINT,
		},
		{
			sequelize,
			modelName: "role_group_has_menu",
		}
	);
	return role_group_has_menu;
};
