"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class sub_menu_has_acces extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			sub_menu_has_acces.belongsTo(models.access_menu, {
				foreignKey: "id_acccess",
				targetKey: "id",
				as: "access_menu",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	sub_menu_has_acces.init(
		{
			id_sub_menu: DataTypes.INTEGER,
			id_acccess: DataTypes.BIGINT,
		},
		{
			sequelize,
			modelName: "sub_menu_has_acces",
		}
	);
	return sub_menu_has_acces;
};
