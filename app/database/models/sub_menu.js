"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class sub_menu extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			sub_menu.hasMany(models.sub_sub_menu, {
				sourceKey: "MenuId",
				onDelete: "CASCADE",
			});
			sub_menu.hasMany(models.sub_menu_has_acces, {
				foreignKey: "id_sub_menu",
				targetKey: "id_sub_menu",
				sourceKey: "id",
				as: "access",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	sub_menu.init(
		{
			name: DataTypes.STRING,
			url: DataTypes.STRING,
			icon: DataTypes.STRING,
			MenuId: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: "sub_menu",
		}
	);
	return sub_menu;
};
