"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class role_group extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			role_group.hasMany(models.role_group_has_menu, {
				foreignKey: "id_role_group",
				targetKey: "id_role_group",
				sourceKey: "id",
				as: "role_group_has_menu",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	role_group.init(
		{
			role_name: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "role_group",
		}
	);
	return role_group;
};
