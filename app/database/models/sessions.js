"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class sessions extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			sessions.belongsTo(models.auth_device, {
				foreignKey: "id_device",
				targetKey: "id",
				sourceKey: "id_device",
				as: "device",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	sessions.init(
		{
			id_auth: DataTypes.STRING,
			id_device: DataTypes.STRING,
			session_token: DataTypes.TEXT,
			login_time: DataTypes.DATE,
			logout_time: DataTypes.DATE,
			ip_address: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "sessions",
		}
	);
	return sessions;
};
