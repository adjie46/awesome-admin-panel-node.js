"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class menu_has_access extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			menu_has_access.belongsTo(models.access_menu, {
				foreignKey: "id_acccess",
				targetKey: "id",
				as: "access_menu",
				onDelete: "CASCADE",
				hooks: true,
			});
		}
	}
	menu_has_access.init(
		{
			id_role_group: DataTypes.INTEGER,
			id_menu: DataTypes.INTEGER,
			id_acccess: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: "menu_has_access",
		}
	);
	return menu_has_access;
};
