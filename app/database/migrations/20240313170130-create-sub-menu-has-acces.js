"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("sub_menu_has_acces", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},

			id_sub_menu: {
				type: Sequelize.INTEGER,
				allowNull: false,
				references: {
					model: "sub_menus",
					id: "id",
				},
			},
			id_acccess: {
				type: Sequelize.BIGINT,
				allowNull: false,
				references: {
					model: "access_menus",
					id: "id",
				},
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("sub_menu_has_acces");
	},
};
