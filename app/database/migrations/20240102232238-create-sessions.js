"use strict";
/** @type {import('sequelize-cli').Migration} */
var dataType = require("sequelize/lib/data-types");
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("sessions", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.BIGINT,
			},
			id_auth: {
				type: Sequelize.UUID,
				defaultValue: dataType.UUID,
				allowNull: false,
				references: {
					model: "auths",
					id: "id",
				},
				onUpdate: "CASCADE",
				onDelete: "CASCADE",
			},
			session_token: {
				type: Sequelize.TEXT,
				allowNull: false,
			},
			login_time: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			logout_time: {
				type: Sequelize.DATE,
			},
			ip_address: {
				type: Sequelize.STRING,
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("sessions");
	},
};
