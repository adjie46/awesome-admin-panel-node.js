"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("role_group_has_menus", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.BIGINT,
			},
			id_role_group: {
				type: Sequelize.BIGINT,
				references: {
					model: "role_groups",
					id: "id",
				},
			},
			id_menu: {
				type: Sequelize.INTEGER,
				references: {
					model: "menus",
					id: "id",
				},
			},
			order_menu: {
				type: Sequelize.BIGINT,
				allowNull: false,
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("role_group_has_menus");
	},
};
