"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("menu_has_accesses", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			id_role_group: {
				type: Sequelize.BIGINT,
				allowNull: false,
				references: {
					model: "role_groups",
					id: "id",
				},
			},
			id_menu: {
				type: Sequelize.INTEGER,
			},
			id_acccess: {
				type: Sequelize.BIGINT,
				allowNull: false,
				references: {
					model: "access_menus",
					id: "id",
				},
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("menu_has_accesses");
	},
};
