"use strict";
/** @type {import('sequelize-cli').Migration} */
var dataType = require("sequelize/lib/data-types");
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("user_has_roles", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.BIGINT,
			},
			id_auth: {
				type: Sequelize.UUID,
				defaultValue: dataType.UUID,
				allowNull: false,
				references: {
					model: "auths",
					id: "id",
				},
				onUpdate: "CASCADE",
				onDelete: "CASCADE",
			},
			id_role_group: {
				type: Sequelize.BIGINT,
				allowNull: false,
				references: {
					model: "role_groups",
					id: "id",
				},
			},
			createdAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
			updatedAt: {
				type: "TIMESTAMP",
				defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
				allowNull: false,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("user_has_roles");
	},
};
