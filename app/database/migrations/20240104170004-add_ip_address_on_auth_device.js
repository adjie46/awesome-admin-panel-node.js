"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	up: async (queryInterface, Sequelize) => {
		// Add the id_session column
		await queryInterface.addColumn("auth_devices", "ip_address", {
			type: Sequelize.TEXT,
			allowNull: true,
			after: "imei_device",
		});
	},

	down: async (queryInterface, Sequelize) => {
		// Remove the id_session column
		await queryInterface.removeColumn("auth_devices", "ip_address");
	},
};
