"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	up: async (queryInterface, Sequelize) => {
		// Add the id_session column
		await queryInterface.addColumn("sessions", "id_device", {
			type: Sequelize.UUID,
			allowNull: true,
			references: {
				model: "auth_devices", // name of the source model
				key: "id", // key in the source model
			},
			after: "id_auth",
			onUpdate: "CASCADE",
			onDelete: "SET NULL", // or 'RESTRICT', depending on your needs
		});
	},

	down: async (queryInterface, Sequelize) => {
		// Remove the id_session column
		await queryInterface.removeColumn("auth_devices", "id_session");
	},
};
