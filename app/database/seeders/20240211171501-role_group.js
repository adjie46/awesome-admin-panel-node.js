"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	up: async (queryInterface, Sequelize) => {
		const menusData = [
			{
				role_name: "Super Admin",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		];

		await queryInterface.bulkInsert("role_groups", menusData, {});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete("role_groups", null, {});
	},
};
