"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	up: async (queryInterface, Sequelize) => {
		const auths = await queryInterface.sequelize.query(
			`SELECT id from auths where user_auth='admin@gmail.com' order by createdAt DESC;`
		);

		const idAuth = auths[0][0].id;

		const menusData = [
			{
				id_auth: idAuth,
				id_role_group: 1,
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		];

		await queryInterface.bulkInsert("user_has_roles", menusData, {});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete("user_has_roles", null, {});
	},
};
