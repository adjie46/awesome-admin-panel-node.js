"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	up: async (queryInterface, Sequelize) => {
		const menusData = [
			{
				access_name: "create",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				access_name: "read",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				access_name: "update",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				access_name: "delete",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		];

		await queryInterface.bulkInsert("access_menus", menusData, {});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete("access_menus", null, {});
	},
};
