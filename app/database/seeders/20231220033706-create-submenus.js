"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	up: async (queryInterface, Sequelize) => {
		const submenusData = [
			{
				name: "Website",
				MenuId: 2,
				url: "/main_setting",
				icon: "custom-story",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				name: "Menu",
				MenuId: 2,
				url: "/menu",
				icon: "custom-row-vertical",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				name: "Hak Akses",
				MenuId: 2,
				url: "/roles_permission",
				icon: "custom-shield",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		];

		await queryInterface.bulkInsert("sub_menus", submenusData, {});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete("sub_menus", null, {});
	},
};
