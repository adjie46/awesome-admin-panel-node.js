"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	up: async (queryInterface, Sequelize) => {
		const menusData = [
			{
				id_role_group: "1",
				id_menu: "1",
				order_menu: "0",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id_role_group: "1",
				id_menu: "2",
				order_menu: "1",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		];

		await queryInterface.bulkInsert("role_group_has_menus", menusData, {});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete("role_group_has_menus", null, {});
	},
};
