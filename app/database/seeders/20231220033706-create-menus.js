"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	up: async (queryInterface, Sequelize) => {
		const menusData = [
			{
				name: "Dashboard",
				url: "/dashboard",
				icon: "custom-home",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				name: "Pengaturan",
				url: "#",
				icon: "custom-setting-2",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		];

		await queryInterface.bulkInsert("menus", menusData, {});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete("menus", null, {});
	},
};
