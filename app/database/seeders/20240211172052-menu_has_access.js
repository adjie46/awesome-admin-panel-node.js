"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
	up: async (queryInterface, Sequelize) => {
		const menusData = [
			{
				id_role_group: "1",
				id_menu: "1",
				id_acccess: "2",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
			{
				id_role_group: "1",
				id_menu: "2",
				id_acccess: "2",
				createdAt: new Date(),
				updatedAt: new Date(),
			},
		];

		await queryInterface.bulkInsert("menu_has_accesses", menusData, {});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete("menu_has_accesses", null, {});
	},
};
