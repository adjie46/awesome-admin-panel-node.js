const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");

module.exports = {
	entry: "./public/assets/js/main.js",
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname, "public/assets/dist"),
	},
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [MiniCssExtractPlugin.loader, "css-loader", "postcss-loader"],
			},
			{
				test: /\.hbs$/,
				loader: "handlebars-loader",
				options: {
					partialDirs: [path.resolve(__dirname, "public/templates/includes")],
					knownHelpers: ["content"],
				},
			},
		],
	},

	plugins: [
		new MiniCssExtractPlugin({
			filename: "bundle.css", // Name of the extracted CSS file
		}),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery",
			Popper: ["popper.js", "default"],
		}),
	],
};
