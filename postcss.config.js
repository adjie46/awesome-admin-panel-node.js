module.exports = {
	plugins: [
		require("cssnano")({
			preset: "default", // Use the default minification settings
		}),
	],
};
