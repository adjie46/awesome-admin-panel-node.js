import "../css/style-preset.css";
import "../css/style.css";
import "../css/main.css";
import "../css/plugins/dataTables.bootstrap5.min.css";
import "../css/plugins/responsive.bootstrap5.min.css";
import "../css/plugins/fixedHeader.bootstrap5.min.css";
import "../css/plugins/fixedColumns.bootstrap5.min.css";

import "../../../node_modules/bootstrap/dist/js/bootstrap.bundle";
import "popper.js";
import "gasparesganga-jquery-loading-overlay";

import "../js/plugins/jquery.dataTables.min";
import "../js/plugins/dataTables.bootstrap5.min";
import "../js/plugins/dataTables.fixedHeader.min";
import "../js/pcoded";

import "../js/custom/core";
import "../js/custom/auth";
import "../js/custom/dashboard";
import "../js/custom/role_group";
import "../js/custom/menu";
