import {
	$,
	m,
	setupAjax,
	password,
	api,
	loadButton,
	notification,
	redirectWithCountdown,
	modal,
} from "./core";

var myId;

$(document).ready(function () {
	var myTable;

	if ($("#tbl_hak_akses").length) {
		myTable = $("#tbl_hak_akses")
			.DataTable({
				// Note the missing '#' in your original selector
				processing: true,
				serverSide: true,
				retrieve: true,
				autoWidth: true, // Note: Correct spelling is 'autoWidth' not 'autoWith'
				paging: true,
				destroy: true,
				responsive: true,
				searching: true,
				ordering: false,
				scrollX: true,
				scrollY: true,
				pageLength: 5,
				lengthMenu: [5, 10, 50, 100, 500],
				ajax: {
					type: "GET",
					url: `/api/role_group`,
					dataSrc: function (json) {
						return json.data;
					},
					error: function (xhr, error, thrown) {
						// Add this error handler
						console.error(`Error occurred: ${thrown}`);
						// You can also inspect the xhr object for more details
					},
				},
				stateSave: true,
				columns: [null, null, null],
				columnDefs: [
					{
						render: function (data) {
							return `
						<td>
            				${data[2]}
        				</td>
					`;
						},
						data: null,
						targets: [0],
					},
					{
						render: function (data) {
							return `
								<button id="btn_hak_akses" data-id='${data[0]}' type="button" class="btn btn-sm btn-outline-success d-inline-flex" data-bs-toggle="modal" data-bs-target="#hak_akses"><i class="ti ti-pencil me-1"></i>Hak Akses</button>
								<button type="button" class="btn btn-sm btn-outline-warning d-inline-flex"><i class="ti ti-sort-ascending-letters me-1"></i>Order Menu</button>
								<button type="button" data-pc-animate="blur" data-bs-toggle="modal" data-bs-target="#edit_role_group" data-id='${data[0]}' id="edt_role_group" class="btn btn-sm btn-outline-primary d-inline-flex"><i class="ti ti-pencil me-1"></i>Edit</button>
								<button id="del_role_group" data-id='${data[0]}' type="button" class="btn btn-sm btn-outline-danger d-inline-flex"><i class="ti ti-trash me-1"></i>Hapus</button>
							`;
						},
						data: null,
						targets: [-1],
					},
				],
			})
			.on("error.dt", function (e, settings, techNote, message) {
				console.error("An error has been reported by DataTables:", message);
				// Perform additional actions or error logging as needed
			});
	}

	$("#frm_add_role_group").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();

			setupAjax();

			var formData = new FormData();
			var params = $(this).serializeArray();

			$.each(params, function (i, val) {
				formData.append(val.name, val.value);
			});

			$(".btn-close").prop("disabled", true);
			$(".btn-outline-secondary").prop("disabled", true);

			api("/api/role_group", "POST", formData)
				.then((response) => {
					if (response.success) {
						setTimeout(() => {
							loadButton("button[type='submit']", 2, "Loading...");

							$("#animateModal").modal("hide");

							Swal.fire("Berhasil!", response.message, "success").then(
								(result) => {
									location.reload();
								}
							);
						}, 100);
					} else {
						setTimeout(() => {
							loadButton("button[type='submit']", 0, "Tambah Hak Akses");

							Swal.fire("Gagal!", response.message, "error").then((result) => {
								Swal.close();
							});

							$(".btn-close").prop("disabled", false);
							$(".btn-outline-secondary").prop("disabled", false);
						}, 100);
					}
				})
				.catch((error) => {
					setTimeout(() => {
						loadButton("button[type='submit']", 0, "Tambah Hak Akses");

						setTimeout(() => {
							$(".alert").delay(5000).slideUp(200);
						}, 500);
					}, 100);
				});
		}
		$(this).addClass("was-validated");
	});

	$(document).on("click", "#btn_hak_akses", function () {
		var id = $(this).attr("data-id");

		$.ajax({
			type: "get",
			headers: {
				"X-CSRF-Token": m.attr("content"),
			},
			url: `/api/role_group/permission/${id}`,
			dataType: "json",
			beforeSend: function () {
				$(".modal-content").LoadingOverlay("show", {
					background: "rgba(165, 190, 100, 0.5)",
				});
			},
			success: function (response) {
				if (response.success) {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#hak_akses_body").html(response.data);
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: response.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
			/*
			error: function (xhr, status, errorThrown) {
				console.log(xhr);
				if (xhr.statusText == "Unauthorized") {
					location.replace("/");
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#edit_role_group").modal("toggle");
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: xhr.responseJSON.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			}, */
		});
	});

	$(document).on("click", "#del_role_group", function () {
		var id = $(this).attr("data-id");
		Swal.fire({
			title: "Yakin ingin menghapus hak akses ini?",
			showCancelButton: true,
			icon: "question",
			confirmButtonText: "Ya, Hapus!",
			showLoaderOnConfirm: true,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: () => {
				return fetch(`/api/role_group/${id}`, {
					method: "delete",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.ok) {
							throw new Error(response.statusText); // Throw an error with the status text
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error.message}`);
						return Promise.reject(error); // Properly handle rejection
					});
			},
			allowOutsideClick: false,
		})
			.then((result) => {
				if (result.isConfirmed) {
					if (result.value && result.value.success) {
						Swal.fire("Berhasil!", result.value.message, "success").then(
							(result) => {
								location.reload();
							}
						);
					} else {
						Swal.fire("Gagal!", result.value.message, "error").then(
							(result) => {
								location.reload();
							}
						);
					}
				} else {
					// Handle the case where the user cancels the action
					// You can add code here to handle cancelation if needed
				}
			})
			.catch((error) => {
				console.error("An error occurred:", error);
			});
	});

	$("#frm_edt_role_group").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();

			setupAjax();

			var formData = new FormData();
			var params = $(this).serializeArray();

			$.each(params, function (i, val) {
				formData.append(val.name, val.value);
			});

			$(".btn-close").prop("disabled", true);
			$(".btn-outline-secondary").prop("disabled", true);

			api(`/api/role_group/${myId}`, "PUT", formData)
				.then((response) => {
					if (response.success) {
						setTimeout(() => {
							loadButton("button[type='submit']", 2, "Loading...");

							$("#animateModal").modal("hide");

							Swal.fire("Berhasil!", response.message, "success").then(
								(result) => {
									location.reload();
								}
							);
						}, 100);
					} else {
						setTimeout(() => {
							loadButton("button[type='submit']", 0, "Tambah Hak Akses");

							Swal.fire("Gagal!", response.message, "error").then((result) => {
								Swal.close();
							});

							$(".btn-close").prop("disabled", false);
							$(".btn-outline-secondary").prop("disabled", false);
						}, 100);
					}
				})
				.catch((error) => {
					setTimeout(() => {
						loadButton("button[type='submit']", 0, "Tambah Hak Akses");

						setTimeout(() => {
							$(".alert").delay(5000).slideUp(200);
						}, 500);
					}, 100);
				});
		}
		$(this).addClass("was-validated");
	});

	$(document).on("click", "#edt_role_group", function (e) {
		var id = $(this).attr("data-id");
		myId = id;

		$.ajax({
			type: "get",
			headers: {
				"X-CSRF-Token": m.attr("content"),
			},
			url: `/api/role_group/${id}`,
			dataType: "json",
			beforeSend: function () {
				$(".modal-content").LoadingOverlay("show", {
					background: "rgba(165, 190, 100, 0.5)",
				});
			},
			success: function (response) {
				if (response.success) {
					console.log(response.data.id);
					$(".modal-content").LoadingOverlay("hide", true);
					$("input[name='_id']").val(response.data.id);
					$("input[name='edt_role_group']").val(response.data.role_name);
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: response.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
			error: function (xhr, status, errorThrown) {
				console.log(xhr);
				if (xhr.statusText == "Unauthorized") {
					location.replace("/");
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#edit_role_group").modal("toggle");
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: xhr.responseJSON.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
		});
	});
	/*  */
});
