import {
	$,
	m,
	setupAjax,
	password,
	api,
	loadButton,
	notification,
	redirectWithCountdown,
	modal,
	ClipboardJS,
} from "./core";

var myId;
var myIcon;
var mode;

$(document).ready(function () {
	var myTable;

	$(document).on("mouseenter", "#lbl_icon", function () {
		// Ketika dihover, ubah ikon kursor menjadi pointer
		$(this).css("cursor", "pointer");
	});

	$(document).on("mouseleave", "#lbl_icon", function () {
		// Ketika tidak dihover, kembalikan ikon kursor ke default
		$(this).css("cursor", "auto");
	});
	$(document).on("click", "#lbl_icon", function () {
		var action = $(this).attr("data-action");
		mode = action;
		$("#icon_modal").modal("show");
		$("#edit_menu").modal("hide");
	});

	if ($("#tbl_menu").length) {
		myTable = $("#tbl_menu")
			.DataTable({
				// Note the missing '#' in your original selector
				processing: true,
				serverSide: true,
				retrieve: true,
				autoWidth: true, // Note: Correct spelling is 'autoWidth' not 'autoWith'
				paging: true,
				destroy: true,
				responsive: true,
				searching: true,
				ordering: false,
				scrollX: true,
				scrollY: true,
				pageLength: 5,
				lengthMenu: [5, 10, 50, 100, 500],
				ajax: {
					type: "GET",
					url: `/api/menu`,
					dataSrc: function (json) {
						return json.data;
					},
					error: function (xhr, error, thrown) {
						// Add this error handler
						console.error(`Error occurred: ${thrown}`);
						// You can also inspect the xhr object for more details
					},
				},
				stateSave: true,
				columns: [null, null, null, null, null],
				columnDefs: [
					{
						render: function (data) {
							return `
						<td>
            				${data[4]}
        				</td>`;
						},
						data: null,
						targets: [0],
					},
					{
						render: function (data) {
							return `
						<td>
            				${data[1]}
        				</td>`;
						},
						data: null,
						targets: [1],
					},
					{
						render: function (data) {
							return `
						<td>
            				${data[2]}
        				</td>`;
						},
						data: null,
						targets: [2],
					},
					{
						render: function (data) {
							return `
						<td>
            				${data[3]}
        				</td>`;
						},
						data: null,
						targets: [3],
					},
					{
						render: function (data) {
							return `
								<button id="btn_sub_menu" type="button" class="btn btn-sm btn-outline-success d-inline-flex"><i class="ti ti-pencil me-1"></i>Sub Menu</button>
								<button type="button" data-pc-animate="blur" data-bs-toggle="modal" data-bs-target="#edit_menu" data-id='${data[0]}' id="edt_menu" class="btn btn-sm btn-outline-primary d-inline-flex"><i class="ti ti-pencil me-1"></i>Edit</button>
								<button id="del_menu" data-id='${data[0]}' type="button" class="btn btn-sm btn-outline-danger d-inline-flex"><i class="ti ti-trash me-1"></i>Hapus</button>
							`;
						},
						data: null,
						targets: [-1],
					},
				],
			})
			.on("error.dt", function (e, settings, techNote, message) {
				console.error("An error has been reported by DataTables:", message);
				// Perform additional actions or error logging as needed
			});

		var icon_list = [
			"custom-home",
			"custom-document",
			"custom-user",
			"custom-box-1",
			"custom-sort-outline",
			"custom-level",
			"custom-notification",
			"custom-setting-2",
			"custom-search-normal-1",
			"custom-layer",
			"custom-sms",
			"custom-document-text",
			"custom-user-bold",
			"custom-security-safe",
			"custom-notification-outline",
			"custom-setting-outline",
			"custom-share-bold",
			"custom-lock-outline",
			"custom-profile-2user-outline",
			"custom-add-outline",
			"custom-logout-1-outline",
			"custom-star-bold",
			"custom-airplane",
			"custom-mouse-circle",
			"custom-refresh-2",
			"custom-simcard-2",
			"custom-facebook",
			"custom-google",
			"custom-github",
			"custom-sun-1",
			"custom-moon",
			"custom-mask",
			"custom-mask-1-outline",
			"custom-bag",
			"custom-note-1",
			"custom-video-play",
			"custom-image",
			"custom-folder-open",
			"custom-user-add",
			"custom-status-up",
			"custom-notification-status",
			"custom-crop",
			"custom-keyboard",
			"custom-graph",
			"custom-kanban",
			"custom-link",
			"custom-login",
			"custom-logout",
			"custom-search-normal",
			"custom-shapes",
			"custom-calendar-1",
			"custom-message-2",
			"custom-shopping-bag",
			"custom-document-filter",
			"custom-direct-inbox",
			"custom-user-square",
			"custom-shield",
			"custom-call-calling",
			"custom-dollar-square",
			"custom-flag",
			"custom-data",
			"custom-presentation-chart",
			"custom-cpu-charge",
			"custom-text-block",
			"custom-row-vertical",
			"custom-document-upload",
			"custom-password-check",
			"custom-24-support",
			"custom-fatrows",
			"custom-element-plus",
			"custom-add-item",
			"custom-clipboard",
			"custom-story",
			"custom-text-align-justify-center",
		];
		for (var i = 0, l = icon_list.length; i < l; i++) {
			let icon_block = document.createElement("div");
			icon_block.setAttribute("class", "i-block");
			icon_block.setAttribute("data-clipboard-text", icon_list[i]);
			icon_block.setAttribute("data-bs-toggle", "tooltip");
			icon_block.setAttribute("data-filter", icon_list[i]);
			icon_block.setAttribute("title", icon_list[i]);

			let icon_main = document.createElement("svg");
			icon_main.setAttribute("class", "pc-icon");

			let icon_symbol = document.createElement("use");
			icon_symbol.setAttribute("xlink:href", "#" + icon_list[i]);

			icon_main.appendChild(icon_symbol);
			icon_block.appendChild(icon_main);
			document.querySelector("#icon-wrapper").append(icon_block);
		}
		window.addEventListener("load", (event) => {
			var cont = document.querySelector("#icon-wrapper").innerHTML;
			document.querySelector("#icon-wrapper").innerHTML = "";
			document.querySelector("#icon-wrapper").innerHTML = cont;

			var i_copy = new ClipboardJS(".i-block");

			i_copy.on("success", function (e) {
				myIcon = e.text;
				$("input[name='icon_menu']").val(myIcon);
				var targetElement = e.trigger;
				let icon_badge = document.createElement("span");
				icon_badge.setAttribute("class", "ic-badge badge bg-success");
				icon_badge.innerHTML = "copied";
				targetElement.append(icon_badge);
				setTimeout(function () {
					targetElement.children[1].remove();
				}, 3000);
				$("#icon_modal").modal("hide");
				if (mode == "edit") {
					$("#edit_menu").modal("show");
				}
			});

			i_copy.on("error", function (e) {
				var targetElement = e.trigger;
				let icon_badge = document.createElement("span");
				icon_badge.setAttribute("class", "ic-badge badge bg-danger");
				icon_badge.innerHTML = "Error";
				targetElement.append(icon_badge);
				setTimeout(function () {
					targetElement.children[1].remove();
				}, 3000);
			});
			document
				.querySelector("#icon-search")
				.addEventListener("keyup", function () {
					var g = document.querySelector("#icon-search").value.toLowerCase();
					var tc = document.querySelectorAll(".i-main .i-block");
					for (var i = 0; i < tc.length; i++) {
						var c = tc[i];
						var t = c.getAttribute("data-filter");
						if (t) {
							var s = t.toLowerCase();
						}
						if (s) {
							var n = s.indexOf(g);
							if (n !== -1) {
								c.style.display = "inline-flex";
							} else {
								c.style.display = "none";
							}
						}
					}
				});
		});
	}

	$("#frm_add_menu").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();

			setupAjax();

			var formData = new FormData();
			var params = $(this).serializeArray();

			$.each(params, function (i, val) {
				formData.append(val.name, val.value);
			});

			$(".btn-close").prop("disabled", true);
			$(".btn-outline-secondary").prop("disabled", true);

			api("/api/menu", "POST", formData)
				.then((response) => {
					if (response.success) {
						setTimeout(() => {
							loadButton("button[type='submit']", 2, "Loading...");

							$("#animateModal").modal("hide");

							Swal.fire("Berhasil!", response.message, "success").then(
								(result) => {
									location.reload();
								}
							);
						}, 100);
					} else {
						setTimeout(() => {
							loadButton("button[type='submit']", 0, "Tambah Menu");

							Swal.fire("Gagal!", response.message, "error").then((result) => {
								Swal.close();
							});

							$(".btn-close").prop("disabled", false);
							$(".btn-outline-secondary").prop("disabled", false);
						}, 100);
					}
				})
				.catch((error) => {
					setTimeout(() => {
						loadButton("button[type='submit']", 0, "Tambah Menu");

						setTimeout(() => {
							$(".alert").delay(5000).slideUp(200);
						}, 500);
					}, 100);
				});
		}
		$(this).addClass("was-validated");
	});

	$(document).on("click", "#del_menu", function () {
		var id = $(this).attr("data-id");
		Swal.fire({
			title: "Yakin ingin menghapus menu ini?",
			showCancelButton: true,
			icon: "question",
			confirmButtonText: "Ya, Hapus!",
			showLoaderOnConfirm: true,
			confirmButtonColor: "#1986C8",
			cancelButtonColor: "#fd625e",
			preConfirm: () => {
				return fetch(`/api/menu/${id}`, {
					method: "delete",
					headers: {
						"X-CSRF-Token": m.attr("content"),
					},
				})
					.then((response) => {
						if (!response.ok) {
							throw new Error(response.statusText); // Throw an error with the status text
						}
						return response.json();
					})
					.catch((error) => {
						Swal.showValidationMessage(`Request failed: ${error.message}`);
						return Promise.reject(error); // Properly handle rejection
					});
			},
			allowOutsideClick: false,
		})
			.then((result) => {
				if (result.isConfirmed) {
					if (result.value && result.value.success) {
						Swal.fire("Berhasil!", result.value.message, "success").then(
							(result) => {
								location.reload();
							}
						);
					} else {
						Swal.fire("Gagal!", result.value.message, "error").then(
							(result) => {
								location.reload();
							}
						);
					}
				} else {
					// Handle the case where the user cancels the action
					// You can add code here to handle cancelation if needed
				}
			})
			.catch((error) => {
				console.error("An error occurred:", error);
			});
	});

	$(document).on("click", "#edt_menu", function (e) {
		var id = $(this).attr("data-id");
		myId = id;

		$.ajax({
			type: "get",
			headers: {
				"X-CSRF-Token": m.attr("content"),
			},
			url: `/api/menu/${id}`,
			dataType: "json",
			beforeSend: function () {
				$(".modal-content").LoadingOverlay("show", {
					background: "rgba(165, 190, 100, 0.5)",
				});
			},
			success: function (response) {
				if (response.success) {
					$(".modal-content").LoadingOverlay("hide", true);
					$("input[name='_id']").val(response.data.id);
					$("input[name='edt_menu_name']").val(response.data.name);
					$("input[name='edt_url_menu']").val(response.data.url);
					$("input[name='icon_menu']").val(response.data.icon);
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: response.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
			error: function (xhr, status, errorThrown) {
				console.log(xhr);
				if (xhr.statusText == "Unauthorized") {
					location.replace("/");
				} else {
					$(".modal-content").LoadingOverlay("hide", true);
					$("#edit_role_group").modal("toggle");
					Swal.fire({
						type: "error",
						title: "<b>Gagal!</b>",
						html: xhr.responseJSON.message,
						confirmButtonText: "OK",
					}).then((result) => {
						location.reload();
					});
				}
			},
		});
	});

	$("#frm_edt_menu").on("submit", function (e) {
		if (!this.checkValidity()) {
			e.preventDefault();
			e.stopPropagation();
		} else {
			e.preventDefault();

			setupAjax();

			var formData = new FormData();
			var params = $(this).serializeArray();

			$.each(params, function (i, val) {
				formData.append(val.name, val.value);
			});

			$(".btn-close").prop("disabled", true);
			$(".btn-outline-secondary").prop("disabled", true);

			api(`/api/menu/${myId}`, "PUT", formData)
				.then((response) => {
					if (response.success) {
						setTimeout(() => {
							loadButton("button[type='submit']", 2, "Loading...");

							$("#edit_menu").modal("hide");

							Swal.fire("Berhasil!", response.message, "success").then(
								(result) => {
									location.reload();
								}
							);
						}, 100);
					} else {
						setTimeout(() => {
							loadButton("button[type='submit']", 0, "Update Menu");

							Swal.fire("Gagal!", response.message, "error").then((result) => {
								Swal.close();
							});

							$(".btn-close").prop("disabled", false);
							$(".btn-outline-secondary").prop("disabled", false);
						}, 100);
					}
				})
				.catch((error) => {
					setTimeout(() => {
						loadButton("button[type='submit']", 0, "Update Menu");

						setTimeout(() => {
							$(".alert").delay(5000).slideUp(200);
						}, 500);
					}, 100);
				});
		}
		$(this).addClass("was-validated");
	});
});
