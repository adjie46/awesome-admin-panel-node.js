import {
	$,
	m,
	setupAjax,
	password,
	api,
	loadButton,
	notification,
	redirectWithCountdown,
} from "./core";

const old_input_pass = document.getElementById("old_password");
const old_pass = document.getElementById("passwordToggle_old");

const new_password = document.getElementById("new_password");
const new_password_toggle = document.getElementById("new_password_toggle");

const confirm_password = document.getElementById("confirm_password");
const confirm_password_toggle = document.getElementById(
	"confirm_password_toggle"
);

if (old_pass) {
	old_pass.addEventListener("click", () => {
		password(old_input_pass, old_pass);
	});
}

if (new_password_toggle) {
	new_password_toggle.addEventListener("click", () => {
		password(new_password, new_password_toggle);
	});
}

if (confirm_password_toggle) {
	confirm_password_toggle.addEventListener("click", () => {
		password(confirm_password, confirm_password_toggle);
	});
}

function activateTabFromURL() {
	const tabId = window.location.hash.substring(1); // Get the fragment identifier without the '#'
	const tabLink = document.querySelector(`[href="#${tabId}"]`);

	if (tabId === null || tabId == "") {
	} else {
		tabLink.click();
	}
}

// Call the function on page load to activate the tab specified in the URL fragment
window.onload = activateTabFromURL;

// Listen for changes in the URL fragment and activate the corresponding tab
window.addEventListener("hashchange", activateTabFromURL);

$("#frm_change_password").on("submit", function (e) {
	if (!this.checkValidity()) {
		e.preventDefault();
		e.stopPropagation();
	} else {
		e.preventDefault();

		setupAjax();

		var formData = new FormData();
		var params = $(this).serializeArray();

		$.each(params, function (i, val) {
			formData.append(val.name, val.value);
		});

		api("/api/user/profile", "PUT", formData)
			.then((response) => {
				if (response.success) {
					setTimeout(() => {
						loadButton("button[type='submit']", 2, "Update Profile");
						notification("#notification", "success", response.message);
						redirectWithCountdown(response.redirect, 3);
					}, 1000);
				} else {
					setTimeout(() => {
						loadButton("button[type='submit']", 0, "Update Profile");
						notification("#notification", "danger", response.message);

						setTimeout(() => {
							$(".alert").delay(5000).slideUp(200);
						}, 500);
					}, 1000);
				}
			})
			.catch((error) => {
				setTimeout(() => {
					loadButton("button[type='submit']", 0, "Update Profile");
					notification("#notification", "danger", error.message);

					setTimeout(() => {
						$(".alert").delay(5000).slideUp(200);
					}, 500);
				}, 1000);
			});
	}
	$(this).addClass("was-validated");
});

$(document).on("click", "#btn_delete_device", function () {
	var id = $(this).attr("data-id");
	Swal.fire({
		title: "Yakin ingin menghapus device ini?",
		showCancelButton: !0,
		type: "warning",
		confirmButtonText: "Ya, hapus!",
		showLoaderOnConfirm: !0,
		confirmButtonColor: "#1986C8",
		cancelButtonColor: "#fd625e",
		preConfirm: async () => {
			return fetch(`/api/user/${id}/delete_device`, {
				method: "delete",
				headers: {
					"X-CSRF-Token": m.attr("content"),
				},
			})
				.then((response) => {
					if (!response.status) {
						throw new Error(response.message);
					}
					return response.json();
				})
				.catch((error) => {
					Swal.showValidationMessage(`Request failed: ${error}`);
				});
		},
		allowOutsideClick: !1,
	}).then((result) => {
		if (result.value.success) {
			Swal.fire("Berhasil!", result.value.message, "success").then((result) => {
				location.reload();
			});
		} else {
			Swal.fire("Gagal!", result.value.message, "error").then((result) => {
				location.reload();
			});
		}
	});
});
