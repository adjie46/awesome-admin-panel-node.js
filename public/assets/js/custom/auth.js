import {
	$,
	setupAjax,
	password,
	notification,
	redirectWithCountdown,
	loadButton,
	api,
} from "./core";

const passwordInput = document.getElementById("password");
const passwordToggle = document.getElementById("passwordToggle");

if (passwordToggle) {
	passwordToggle.addEventListener("click", () => {
		password(passwordInput, passwordToggle);
	});
}

$("#frm_login").on("submit", function (e) {
	if (!this.checkValidity()) {
		e.preventDefault();
		e.stopPropagation();
	} else {
		e.preventDefault();

		setupAjax();

		var formData = new FormData();
		var params = $(this).serializeArray();

		$.each(params, function (i, val) {
			formData.append(val.name, val.value);
		});

		api("/api/auth/login", "POST", formData)
			.then((response) => {
				if (response.success) {
					setTimeout(() => {
						loadButton("button[type='submit']", 2, "Login");
						notification("#notification", "success", response.message);
						redirectWithCountdown(response.redirect, 3);
					}, 1000);
				} else {
					setTimeout(() => {
						loadButton("button[type='submit']", 0, "Login");
						notification("#notification", "danger", response.message);

						setTimeout(() => {
							$(".alert").delay(5000).slideUp(200);
						}, 500);
					}, 1000);
				}
			})
			.catch((error) => {
				setTimeout(() => {
					loadButton("button[type='submit']", 0, "Login");
					notification("#notification", "danger", error.message);

					setTimeout(() => {
						$(".alert").delay(5000).slideUp(200);
					}, 500);
				}, 1000);
			});
	}
	$(this).addClass("was-validated");
});
