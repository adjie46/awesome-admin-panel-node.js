import $ from "jquery";
import ClipboardJS from "../plugins/clipboard.min";
import bootstrap from "../../../../node_modules/bootstrap/dist/js/bootstrap.bundle";

export { $, ClipboardJS, bootstrap };

var m = $("meta[name=X-ID-Compatible]");
export { m };

export function setupAjax() {
	$.ajaxSetup({
		headers: {
			"X-CSRF-Token": $("input[name=_csrf]").val(),
		},
	});
}

export function password(passwordInput, passwordToggle) {
	if (passwordInput.type === "password") {
		passwordInput.type = "text";
		passwordToggle.classList.remove("far-eye");
		passwordToggle.classList.add("far-eye-slash");
	} else {
		passwordInput.type = "password";
		passwordToggle.classList.remove("far-eye-slash");
		passwordToggle.classList.add("far-eye");
	}
}

export function notification(id, type, message) {
	const $notification = $(id).html(`
        <div class="alert customize-alert alert-dismissible text-${type} border border-${type} fade show remove-close-icon" role="alert">
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            <div class="d-flex align-items-center font-medium me-3 me-md-0">
                <i class="ti ti-info-circle fs-5 me-2 flex-shrink-0 text-${type}"></i>
                ${message} <div id="redirect"/>
            </div>
        </div>`);

	// Add a click event handler to close the notification with a slide-up animation
	$notification.find(".btn-close").on("click", function () {
		$notification.slideUp(200, function () {
			$(this).alert("close");
		});
	});

	// Display the notification initially
	$notification.slideDown(200);
}

export function redirectWithCountdown(url, countdownInSeconds) {
	// Membuat elemen HTML untuk menampilkan penghitung mundur
	const countdownElement = document.createElement("div");
	countdownElement.innerHTML = `,akan dialihkan dalam <span id="countdown">${countdownInSeconds}</span> detik.`;

	const notificationElement = document.querySelector("#redirect");
	notificationElement.appendChild(countdownElement);

	// Membuat penghitung mundur
	let countdown = countdownInSeconds;
	const countdownInterval = setInterval(() => {
		countdown--;
		document.getElementById("countdown").textContent = countdown;

		// Jika penghitung mundur mencapai 0, maka alihkan halaman
		if (countdown === 0) {
			clearInterval(countdownInterval); // Menghentikan penghitung mundur
			window.location.href = url; // Melakukan pengalihan ke URL yang ditentukan
		}
	}, 1000); // Penghitung mundur setiap 1 detik (1000 ms)
}

export function loadButton(idButton, type, text) {
	if (type == 1) {
		$(idButton).prop("disabled", true);
		$(idButton).html(
			`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading`
		);
	} else if (type == 0) {
		$(idButton).prop("disabled", false);
		$(idButton).html(text);
	} else if (type == 2) {
		$(idButton).prop("disabled", true);
		$(idButton).html(text);
	}
}

export async function modal(id) {
	var animateModal = document.getElementById(id);
	animateModal.addEventListener("show.bs.modal", function (event) {
		var button = event.relatedTarget;
		var recipient = button.getAttribute("data-pc-animate");
		var modalTitle = animateModal.querySelector(".modal-title");

		animateModal.classList.add("anim-" + recipient);
		if (
			recipient == "let-me-in" ||
			recipient == "make-way" ||
			recipient == "slip-from-top"
		) {
			document.body.classList.add("anim-" + recipient);
		}
	});
	animateModal.addEventListener("hidden.bs.modal", function (event) {
		removeClassByPrefix(animateModal, "anim-");
		removeClassByPrefix(document.body, "anim-");
	});

	function removeClassByPrefix(node, prefix) {
		for (let i = 0; i < node.classList.length; i++) {
			let value = node.classList[i];
			if (value.startsWith(prefix)) {
				node.classList.remove(value);
			}
		}
	}
}

export async function api(url, type, data) {
	try {
		loadButton("button[type='submit']", 1);
		const response = await $.ajax({
			type: type,
			url: url,
			data: data,
			dataType: "json",
			contentType: false,
			processData: false,
		});

		return response;
	} catch (error) {
		if (error.statusText === "Unauthorized") {
			location.replace("/");
		} else if (error.responseJSON && error.responseJSON.status === 2) {
			location.replace("/maintenance");
		} else {
			return error;
		}
		throw error;
	}
}
