const postcss = require("postcss");
const obfuscator = require("postcss-obfuscator");

postcss([
	obfuscator({
		/* options */
	}),
])
	.process(css)
	.then((result) => {
		console.log("Task Completed!", result);
	})
	.catch((error) => {
		console.error(error);
	});
