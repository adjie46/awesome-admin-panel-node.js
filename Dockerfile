# Menggunakan image Node.js sebagai dasar
FROM node:18

# Menyisipkan direktori proyek Anda ke dalam image
WORKDIR /usr/src/app
COPY package*.json ./

# Install dependensi proyek Anda
RUN npm install

# Menyalin seluruh kode sumber aplikasi Anda ke dalam image
COPY . .

# Port yang akan di-ekspos oleh kontainer
EXPOSE 8080

# Perintah yang akan dijalankan ketika kontainer dimulai
CMD ["npx", "concurrently", "webpack --mode development --watch", "npm run server"]